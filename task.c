#include "task.h"
#include<stdio.h>
void init_queue()
{
    //initialize ready queue as idle
    for(i=0; i<TASKS_COUNT; i++)
    {
        ready_queue[i].entry=TASK_idle_task;
        ready_queue[i].id=idle_task;
        ready_queue[i].static_priority=0;

        suspend[i].entry=TASK_idle_task;
        suspend[i].id=idle_task;
        suspend[i].static_priority=0;
    }
    //put all tasks in suspended queue
    for(i=0; i<TASKS_COUNT; i++)
    {
        suspend[i].entry=task_const[i].entry;
        suspend[i].id=task_const[i].id;
        suspend[i].static_priority=task_const[i].static_priority;
    }

    //put task in auto_start_list into ready queue
    for(i=0; i<AUTO_START_TASKS_COUNT; i++)
    {
        if(auto_start_tasks_list[i]==idle_task)continue;
        int k=0;
        for(k=0; k<TASKS_COUNT; k++)
        {
            if(ready_queue[k].id==idle_task)
            {
                ready_queue[k].id=auto_start_tasks_list[i];
                break;
            }
        }

        int j=0;
        for(j=0; j<TASKS_COUNT; j++)
        {
            if(ready_queue[k].id==task_const[j].id)
            {
                ready_queue[k].entry=task_const[j].entry;
                ready_queue[k].static_priority=task_const[j].static_priority;
                break;
            }
        }
        for(j=0; j<TASKS_COUNT; j++)
        {
            if(ready_queue[k].id==suspend[j].id)
            {
                suspend[j].entry=TASK_idle_task;
                suspend[j].id=idle_task;
                suspend[j].static_priority=0;
                break;
            }
        }
    }
    //nothing is runnung
    running.entry=TASK_idle_task;
    running.id=idle_task;
    running.static_priority=0;

    //initial other variables
    count=0;
    for(i=0; i<TASKS_COUNT; i++)
    {
        first[i]=0;
        terminate[i]=0;
    }
    for(i=0; i<100; i++)
    {
        transfer[i][0]=-1;
        transfer[i][1]=-1;
    }
}

void contextswitch(int condition)
{
    /*for(i=0;i<TASKS_COUNT;i++){
        printf("%d",ready_queue[i].id);
    }*/
    //printf("condition%d",condition);
    int priority=0;
    int index=0;
    int same=0;

    task_const_type stack[TASKS_COUNT][8192];
    task_type origianl=running.id;

    //get highest priority in ready queue
    for(i=0; i<TASKS_COUNT; i++)
    {
        if(ready_queue[i].static_priority>=priority)
        {
            if(ready_queue[i].static_priority==priority)same++;
            if(ready_queue[i].static_priority>priority)same=0;
            index=i;
            priority=ready_queue[i].static_priority;
        }
    }

    //decide to do contextswitch or not
    if(condition==3);   //if task terminates,must contextswitch

    if(condition!=3)    //other conditon ,check whether task in ready_queue has higher priority than running task
    {
        if(priority<=running.static_priority)return;
    }

    //start contextswitch
    //send the running task back to ready queue
    for(i=0; i<TASKS_COUNT; i++)
    {
        if(condition==3)break;
        if(ready_queue[i].id==idle_task)
        {
            ready_queue[i].entry=running.entry;
            ready_queue[i].id=running.id;
            ready_queue[i].static_priority=running.static_priority;
            break;
        }
    }
    //set new running task
    if(same==0)
    {
        running.entry=ready_queue[index].entry;
        running.id=ready_queue[index].id;
        running.static_priority=ready_queue[index].static_priority;
    }
    else
    {
        //if multiple tasks have highest priority , choose the one which occupy the resource first
        for(i=0; i<TASKS_COUNT; i++)
        {
            if(ready_queue[i].static_priority==priority)
            {
                int j=0;
                for(j=0; j<RESOURCES_COUNT; j++)
                {
                    if(ready_queue[i].id==resource[j].id)
                    {
                        index=i;
                        running.entry=ready_queue[i].entry;
                        running.id=ready_queue[i].id;
                        running.static_priority=ready_queue[i].static_priority;
                        break;
                    }
                }
            }
        }
        //if multiple tasks have highest priority , but none of them occupy resource
        if(origianl==running.id)
        {
            for(i=0; i<TASKS_COUNT; i++)
            {
                if(ready_queue[i].static_priority==priority)
                {
                    index=i;
                    running.entry=ready_queue[i].entry;
                    running.id=ready_queue[i].id;
                    running.static_priority=ready_queue[i].static_priority;
                    break;
                }
            }
        }
    }

    for(count=0; count<100; count++)
    {
        if(transfer[count][0]==-1)
        {
            transfer[count][0]=origianl;
            transfer[count][1]=running.id;
            break;
        }
    }

    /*for(count=0;count<100;count++){
        printf("%d %d",transfer[count][0],transfer[count][1]);
        printf("\n");
        if(transfer[count][0]==-1){
            break;
        }
    }*/

    //remove from ready queue
    for(i=index; i<TASKS_COUNT-1; i++)
    {
        ready_queue[i]=ready_queue[i+1];
    }
    //printf("new:%d\n",running.id);

    //if task is first time do contextswitch , get and make context
    if(first[running.id]==0||terminate[running.id]==1)
    {
        if(terminate[running.id]==1)terminate[running.id]=0;
        //printf("first   original:%d",origianl);
        first[running.id]=1;

        getcontext(&cur[origianl]);

        cur[origianl].uc_link=NULL;
        cur[origianl].uc_stack.ss_sp=stack[origianl];
        cur[origianl].uc_stack.ss_size=sizeof(stack[origianl]);

        makecontext(&cur[origianl],running.entry,0);

        swapcontext(&cur[running.id],&cur[origianl]);
    }
    else
    {
        //printf("not first %d %d ",running.id,origianl);

        for(i=0; i<100; i++)
        {
            if(transfer[i][0]==running.id)count=i;
        }
        //printf("%d\n",transfer[count][1]);


        swapcontext(&cur[running.id],&cur[transfer[count][1]]);
    }
}

status_type activate_task(task_type id)
{
    //if task has already in ready queue,ignore the request
    for(i=0; i<TASKS_COUNT; i++)
    {
        if(ready_queue[i].id==id)return STATUS_ERROR;
    }
    //if task active itself
    if(running.id==id)return STATUS_ERROR;

    //search the task in suspended queue
    for(i=0; i<TASKS_COUNT; i++)
    {
        //if find
        if(suspend[i].id==id)
        {
            int k;
            for(k=0; k<TASKS_COUNT; k++)
            {
                if(ready_queue[k].id==idle_task)break;
            }
            ready_queue[k].entry=suspend[i].entry;
            ready_queue[k].id=suspend[i].id;
            ready_queue[k].static_priority=suspend[i].static_priority;

            suspend[i].entry=TASK_idle_task;
            suspend[i].id=idle_task;
            suspend[i].static_priority=0;
            break;
        }
        //if not find in suspended queue
        if(i==TASKS_COUNT-1)return STATUS_ERROR;
    }
    contextswitch(2);
    status_type result = STATUS_OK;

    return result;
}

status_type terminate_task(void)
{
    //check whether the calling task occupies any resource
    for(i=0; i<RESOURCES_COUNT; i++)
    {
        if(resource[i].id==running.id)
        {
            return STATUS_ERROR;
        }
    }

    for(i=0; i<TASKS_COUNT; i++)
    {
        if(suspend[i].id==idle_task)
        {
            suspend[i].entry=running.entry;
            suspend[i].id=running.id;
            suspend[i].static_priority=running.static_priority;
            break;
        }
    }
    terminate[running.id]=1;

    contextswitch(3);

    status_type result = STATUS_OK;

    return result;
}