#include "resource.h"
#include<stdio.h>

void init_resource()
{
    for(i=0; i<RESOURCES_COUNT; i++)
    {
        resource[i].entry=TASK_idle_task;
        resource[i].id=idle_task;
        resource[i].static_priority=0;
    }
}

status_type get_resource(resource_type id)
{
    if(resource[id].id==idle_task)
    {
        resource[id].entry=running.entry;
        resource[id].id=running.id;
        resource[id].static_priority=running.static_priority;

        //if task`s priority is lower than resource`s,reset it
        running.static_priority=resources_priority[id];
    }
    else
    {
        //printf("%d",resource[id].id);
        return STATUS_ERROR;
    }
    status_type result = STATUS_OK;

    return result;
}

status_type release_resource(resource_type id)
{
    //printf("%d",running.id);
    if(resource[id].id!=running.id)return STATUS_ERROR;
    else
    {
        resource[id].entry=TASK_idle_task;
        resource[id].id=idle_task;
        //when release resource reset task`s priority
        running.static_priority=resource[id].static_priority;
        //printf("%d\n%d",running.id,running.static_priority);
        resource[id].static_priority=0;
    }
    contextswitch(4);
    status_type result = STATUS_OK;

    return result;
}