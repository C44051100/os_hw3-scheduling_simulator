#ifndef TASK_H
#define TASK_H

#include "typedefine.h"
#include "config.h"
#include "task_set.h"
#include "resource.h"

#include <ucontext.h>

status_type activate_task(task_type id);
status_type terminate_task(void);

task_const_type ready_queue[TASKS_COUNT];
task_const_type suspend[TASKS_COUNT];
task_const_type running;
int i;
void init_queue();
void choose_and_execute();
void contextswitch(int condition); //condition: 1=start,2=active task,3=terminate task,4=release resource

ucontext_t task_counter[TASKS_COUNT];
ucontext_t cur[TASKS_COUNT];
ucontext_t next[TASKS_COUNT];

int first[TASKS_COUNT];
int terminate[TASKS_COUNT];
int count;
int transfer[100][2];

#endif /* TASK_H */