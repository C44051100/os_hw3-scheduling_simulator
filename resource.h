#ifndef RESOURCE_H
#define RESOURCE_H

#include "typedefine.h"
#include "config.h"
#include "task_set.h"
#include "task.h"

status_type get_resource(resource_type id);
status_type release_resource(resource_type id);

task_const_type resource[RESOURCES_COUNT];
int i;

void init_resource();

#endif /* RESOURCE_H */